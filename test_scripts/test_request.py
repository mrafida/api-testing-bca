from flask import jsonify
import requests

def test_initiate():
	r = requests.post("http://127.0.0.1:2222/generateEmailFromTemplate/initiate",
		json={
		"EmailID": "TESPIM01_2AB7925O",
		"TemplateCode": "H2H_FUND_BLOKIR_NOTIF_EMAIL",
		"Email": {
			"EmailTo": "muhammad.amrullah@prosa.ai",
			"EmailCc": "fidrafid@gmail.com",
			"EmailBcc": "13515125@std.stei.itb.ac.id",
			"EmailCustomSubject": "Notifikasi Email"
			},
		"Parameter": [
			{
			"Key": "corp_id_in",
			"Value": "Corp Value Sample"
			},{
				"Key": "id_in",
				"Value": "ID Value Sample"
			}],
		"Attachment": [
			{
			"FileName": "file 01.pdf"
			},{
			"FileName": "file 02.pdf"
			}]
		},
		headers={
		"ClientID": "6E061FF6F71A34EAE05400144FFA3B5D"
		})

	result = r.status_code == 200
	result = result and (r.json().get("ErrorSchema").get("ErrorCode") == "ESB-00-000")
	assert result

def test_resend():
	r = requests.post("http://127.0.0.1:2222/generateEmailFromTemplate/resend",
		json={
		"EmailID": "TESPIM01_2AB7925O"
		},
		headers={
		"ClientID": "6E061FF6F71A34EAE05400144FFA3B5D"
		})

	result = r.status_code == 200
	result = result and (r.json().get("ErrorSchema").get("ErrorCode") == "ESB-00-000")
	assert result