from flask import Flask, jsonify, request, render_template
from flask_mail import Mail, Message
import sqlite3, ast, magic, requests

app = Flask(__name__)
app.config["DEBUG"] = True

app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 587
app.config["MAIL_USE_TLS"] = True
app.config["MAIL_USE_SSL"] = False
app.config["MAIL_USERNAME"] = "gamifidcation@gmail.com"
app.config["MAIL_PASSWORD"] = "gamifidcation123"
app.config["MAIL_DEFAULT_SENDER"] = "gamifidcation@gmail.com"
app.config["MAIL_MAX_EMAILS"] = None
app.config["MAIL_ASCII_ATTACHMENTS"] = False
# app.config["MAIL_DEBUG"] = 
# app.config["MAIL_SUPPRESS_SEND"] = 

mail = Mail(app)

@app.route('/', methods=['GET'])
def get():
	try:
		con = sqlite3.connect("database.db")
		con.row_factory = sqlite3.Row
		cur = con.cursor()
		cur.execute("SELECT * FROM emails")
		rows = cur.fetchall()
		response = []

		for row in rows:
			response.append({
				'email_id': row["email_id"],
				'template_code': row["template_code"],
				'email_to': row["email_to"],
				'email_cc': row["email_cc"],
				'email_bcc': row["email_bcc"],
				'email_subject': row["email_subject"],
				'parameter': ast.literal_eval(row["parameter"]),
				'attachment': ast.literal_eval(row["attachment"]),
				'is_sent': row["is_sent"]
			})
	except:
		response = {
		"ErrorCode": "ESB-XX-XXX",
		"ErrorMessage": {"Indonesian": "Gagal dalam membaca data", "English": "Error in read operation"}
		}
	finally:
		con.close()
		return jsonify(response)

@app.route('/generateEmailFromTemplate/initiate', methods=['POST'])
def initiate():
	header = request.headers.get("ClientID")

	try:
		# initiate_(request)

		email_id = request.json.get("EmailID")
		template_code = request.json.get("TemplateCode")
		email_to = request.json.get("Email").get("EmailTo")
		email_cc = request.json.get("Email").get("EmailCc")
		email_bcc = request.json.get("Email").get("EmailBcc")
		email_subject = request.json.get("Email").get("EmailCustomSubject")
		parameter = str(request.json.get("Parameter"))
		attachment = str(request.json.get("Attachment"))
		is_sent = False

		con = sqlite3.connect("database.db")
		cur = con.cursor()
		cur.execute("INSERT INTO emails (email_id,template_code,email_to,email_cc,"\
			"email_bcc,email_subject,parameter,attachment,is_sent) VALUES (?,?,?,?,?,?,?,?,?)",
			(email_id,template_code,email_to,email_cc,email_bcc,email_subject,parameter,attachment,is_sent))
		result = {
			"ErrorSchema": {
				"ErrorCode": "ESB-00-000",
				"ErrorMessage": {"Indonesian": "Berhasil", "English": "Success"}
				}
			}
	except:
		con.rollback()

		# f = open("log.txt","w")
		# f.write(str(r.json()))
		# f.close()

		result = {
			"ErrorCode": "ESB-XX-XXX",
			"ErrorMessage": {"Indonesian": "Gagal dalam memasukan data", "English": "Error in insert operation"}
			}
	finally:
		con.commit()
		con.close()
		return jsonify(result)

@app.route('/generateEmailFromTemplate/resend', methods=['POST'])
def send():
	try:
		# send_(request)

		con = sqlite3.connect("database.db")
		con.row_factory = sqlite3.Row
		cur = con.cursor()
		cur.execute("SELECT * FROM emails WHERE email_id = \""+request.json.get("EmailID")+"\"")
		rows = cur.fetchall()[0]

		data = {
			'email_id': rows["email_id"],
			'template_code': rows["template_code"],
			'email_to': rows["email_to"],
			'email_cc': rows["email_cc"],
			'email_bcc': rows["email_bcc"],
			'email_subject': rows["email_subject"],
			'parameter': ast.literal_eval(rows["parameter"]),
			'attachment': ast.literal_eval(rows["attachment"])
		}

		# example data
		role = "admin"
		percentage = "50%"
		image_path = "http://127.0.0.1:2222/static/graph.png"
		dashboard_url = "http://35.198.193.81:8000"

		msg = Message(subject=data.get('email_subject'), recipients=[data.get('email_to')],
			cc=[data.get('email_cc')], bcc=[data.get('email_bcc')])
		msg.html = render_template(data.get('template_code')+".html",
			email_to=data.get('email_to'), role=role, percentage=percentage, image_path=image_path, dashboard_url=dashboard_url)

		for file in data.get("attachment"):
			with app.open_resource("attachments/"+file.get("FileName")) as f:
				msg.attach(file.get("FileName"), magic.from_file("attachments/"+file.get("FileName"), mime=True),f.read())
		mail.send(msg)

		cur.execute("UPDATE emails SET is_sent = 1 WHERE email_id = \""+data.get('email_id')+"\"")
		result = {
			"ErrorSchema": {
				"ErrorCode": "ESB-00-000",
				"ErrorMessage": {"Indonesian": "Berhasil", "English": "Success"}
				}
			}
	except:
		con.rollback()
		result = {
			"ErrorCode": "ESB-XX-XXX",
			"ErrorMessage": {"Indonesian": "Gagal dalam mengirim email", "English": "Error in sending email"}
			}
	finally:
		con.commit()
		con.close()
		return jsonify(result)

# Fungsi untuk request ke API BCA

# def initiate_(data):
# 	r = requests.post("http://10.20.215.10:8201/generateEmailFromTemplate/initiate",
# 		json=dict(data.json),
# 		headers={
# 		"ClientID": data.headers.get("ClientID")
# 		})

# def send_(data):
# 	r = requests.post("http://10.20.215.10:8201/generateEmailFromTemplate/resend",
# 		json={
# 		"EmailID": data.json.get("EmailID")
# 		},
# 		headers={
# 		"ClientID": data.headers.get("ClientID")
# 		})

app.run(port=2222)