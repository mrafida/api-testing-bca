import sqlite3

conn = sqlite3.connect('database.db')
print "Opened database successfully";

conn.execute('CREATE TABLE emails (email_id TEXT, template_code TEXT, email_to TEXT, '\
	'email_cc TEXT, email_bcc TEXT, email_subject TEXT, parameter TEXT, attachment TEXT, is_sent BINARY)')

print "Table created successfully";
conn.close()