API Testing BCA
===============


**Development:**

1. run `python init_db.py`
2. run Postman
3. send POST request to `http://127.0.0.1:2222/generateEmailFromTemplate/initiate` to initialize an email
4. send POST request to `http://127.0.0.1:2222/generateEmailFromTemplate/resend` to send an initialized email
5. send GET request to `http://127.0.0.1:2222` to read local email database


Sample body request to initialize an email:

```
{
	"EmailID": "TESPIM01_2AB7925O",
	"TemplateCode": "H2H_FUND_BLOKIR_NOTIF_EMAIL",
	"Email": {
		"EmailTo": "muhammad.amrullah@prosa.ai",
		"EmailCc": "fidrafid@gmail.com",
		"EmailBcc": "13515125@std.stei.itb.ac.id",
		"EmailCustomSubject": "Email Notifikasi"
	},
	"Parameter": [{
		"Key": "corp_id_in",
		"Value": "Corp Value Sample"
	},{
		"Key": "id_in",
		"Value": "ID Value Sample"
	}
	],
	"Attachment": [{
		"FileName": "file 01.pdf"
	},{
		"FileName": "file 02.pdf"
	}]
}
```

Sample body request to send an initialized email:

```
{
	"EmailID": "TESPIM01_2AB7925O"
}
```

**Testing:**

run `pytest`, to run the test script
